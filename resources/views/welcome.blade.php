@extends('layouts.app')

@section('content')
		<div class="container">
			<div class="text-center mt-5">
				<div class="jumbotron jumbotron-fluid">
					<div class="container">
						<h1 class="display-4">REGISTER COMPANY</h1>
						<p class="lead">EXAMPLE REGISTER COMPANY WEB APPLICATION</p>
					</div>
					<a href="/company/create" class="btn btn-danger">COMPANY</a>
					<a href="/" class="btn btn-danger">EMPLOYEE</a>
					<div class="mt-5">
						<small class="text-muted">
							Copyright © 2019 AKRP MIT. All rights reserved. Powered By AKRP.
						</small>
					</div>
				</div>
			</div>
		</div>
@endsection
