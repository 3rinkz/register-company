@extends('layouts.app')

@section('content')
	<div class="container mt-5">
		<h5>Create Company</h5>
		<small class="text-muted">If you already company account please click <a href="/">back to login </a></small>
		<form class="mt-2" method="post" action="{{ route('company.store') }}">
			<div style="width: 400px">
				<div class="form-group">
					<label for="companyName">Name</label>
					@csrf
					<input type="text" name="company_name" id="companyName" class="form-control" placeholder="Fill Company Name..." />
				</div>
				<div class="form-group">
					<label for="userName">Username</label>
					<input type="text" name="username" id="userName" class="form-control" placeholder="Fill Username..." />
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" class="form-control" placeholder="Fill Password..." />
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-danger">Submit</button>
					<button type="submit" class="btn btn-dark">Clear</button>
				</div>
			</div>
		</form>
	</div>
@endsection
